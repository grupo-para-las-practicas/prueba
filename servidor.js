const express = require('express');
const app = express();
const puerto = 8032;

app.get('/', (req, res)=>{
  res.send("<html><h2>¡Hola mundo!</h2></html>");
  visitas++;
});
app.get('/ruta1', (req, res)=>{
    res.send("<html><h4>Esta es la ruta 1!</h4></html>");
    visitas++;
});
app.get('/ruta2', (req, res)=>{
    res.send("<html><h4>Esta es la ruta 2!</h4></html>");
    visitas++;
});
app.listen(puerto, function() {
  console.log("El servidor está activo en http://localhost:8032/");
});




